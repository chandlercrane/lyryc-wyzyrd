import sys
import csv

from sklearn.model_selection import train_test_split

artist_dict = {}

with open('./data/all-songs-fixed.csv') as csv_file:
	csv_reader = csv.reader(csv_file, delimiter=',')
	song_list = list(csv_reader)[1:]

for song in song_list:
	a = song[3]
	if a not in artist_dict.keys():
		artist_dict[a] = 1
	else:
		artist_dict[a] += 1

train_lyrics, test_lyrics = train_test_split(song_list, test_size=0.10, random_state=69)

### ALL ARTISTS ###
## make train.csv ##
with open('./data/all-artists/train.csv', 'w', newline='') as csv_file:
	csv_writer = csv.writer(csv_file, delimiter=',')
	for song in train_lyrics:
		csv_writer.writerow(song)
## make test.csv ##
with open('./data/all-artists/test.csv', 'w', newline='') as csv_file:
	csv_writer = csv.writer(csv_file, delimiter=',')
	for song in test_lyrics:
		csv_writer.writerow(song)
## make genre and country files
for song in train_lyrics:
	index = song[0]
	title = " ".join(song[1].lower().split('-'))
	year = song[2]
	artist = song[3]
	artist_text = " ".join(song[3].lower().split('-'))
	genre = song[4].lower()
	lyrics = song[5]

	# append to genre file
	genre_file = "./data/all-artists/genre/" + genre
	g = open(genre_file, "a+")
	g.write(lyrics)
	g.close()

	# append to year file
	year_file = "./data/all-artists/year/" + year
	y = open(year_file, "a+")
	y.write(lyrics)
	y.close()

### TOP ARTISTS ###
MINIMUM_SONG_LIMIT = 20
## make train.csv ##
with open('./data/top-artists/train.csv', 'w', newline='') as csv_file:
	csv_writer = csv.writer(csv_file, delimiter=',')
	for song in train_lyrics:
		a = song[3]
		if a in artist_dict.keys():
			if artist_dict[a] > MINIMUM_SONG_LIMIT:
					csv_writer.writerow(song)
## make test.csv ##
with open('./data/top-artists/test.csv', 'w', newline='') as csv_file:
	csv_writer = csv.writer(csv_file, delimiter=',')
	for song in test_lyrics:
		a = song[3]
		if a in artist_dict.keys():
			if artist_dict[a] > MINIMUM_SONG_LIMIT:
				csv_writer.writerow(song)
## make artist files ##
for song in train_lyrics:
	index = song[0]
	title = " ".join(song[1].lower().split('-'))
	year = song[2]
	artist = song[3]
	artist_text = " ".join(song[3].lower().split('-'))
	genre = song[4].lower()
	lyrics = song[5]
	if artist in artist_dict.keys():
		if artist_dict[artist] > MINIMUM_SONG_LIMIT:
			# append to artist file
			artist_file = "./data/top-artists/artist/" + artist
			a = open(artist_file, "a+")
			a.write(lyrics)
			a.close()
