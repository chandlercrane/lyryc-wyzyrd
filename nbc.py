#!/usr/bin/env python

# Chandler Crane and Sophie Johnson
# Naive Bayes Classifier

# NLP Project: Baseline
# 1) Count word frequencies for each genre given a text file with lyrics.
# 2) Calculate log probabilities given the word frequencies for each genre.
# 3) Find probability of each genre in test data by adding its log prob for that word.

import os
import math
import ncsc
import csv
import pickle
from sklearn.metrics import mean_squared_error
from sklearn.metrics import classification_report
from nltk.stem import PorterStemmer

REMOVE_STOP_WORDS   = True
USE_STEMMING        = True
TO_LOWERCASE        = True

def getStopWords():
    return set(['ourselves', 'hers', 'between', 'yourself', 'but', 'again', 'there', 'about', 'once', 'during', 'out', 'very', 'having', 'with', 'they', 'own', 'an', 'be', 'some', 'for', 'do', 'its', 'yours', 'such', 'into', 'of', 'most', 'itself', 'other', 'off', 'is', 's', 'am', 'or', 'who', 'as', 'from', 'him', 'each', 'the', 'themselves', 'until', 'below', 'are', 'we', 'these', 'your', 'his', 'through', 'don', 'nor', 'me', 'were', 'her', 'more', 'himself', 'this', 'down', 'should', 'our', 'their', 'while', 'above', 'both', 'up', 'to', 'ours', 'had', 'she', 'all', 'no', 'when', 'at', 'any', 'before', 'them', 'same', 'and', 'been', 'have', 'in', 'will', 'on', 'does', 'yourselves', 'then', 'that', 'because', 'what', 'over', 'why', 'so', 'can', 'did', 'not', 'now', 'under', 'he', 'you', 'herself', 'has', 'just', 'where', 'too', 'only', 'myself', 'which', 'those', 'i', 'after', 'few', 'whom', 't', 'being', 'if', 'theirs', 'my', 'against', 'a', 'by', 'doing', 'it', 'how', 'further', 'was', 'here', 'than'])

# Train naive bayes
def trainNBC(genres, filePath):

    freq         = {genre:{} for genre in genres}
    totalFreq    = {}
    allGenreFreq = 0

    # Record word frequencies for each genre
    for genre in genres:
        for line in open(filePath + str(genre)):
            for word in line.rstrip().split():
                if REMOVE_STOP_WORDS and word in stopWords:
                    continue
                if USE_STEMMING:
                    word = ps.stem(word)
                # Record specific word count in genre (add 1 smoothing)
                freq[genre][word] = freq[genre].get(word, 1) + 1
                # Record count of all words in genre
                totalFreq[genre] = totalFreq.get(genre, 0) + 1
                # Record all words total
                allGenreFreq += 1

    # Calculate probability of each genre
    genreProbs = {}
    for genre in genres:
        genreProbs[genre] = math.log(totalFreq[genre] / allGenreFreq)

    # Calculate word probabilities for each genre
    wordProbs = {genre:{} for genre in genres}
    for genre, freqs in freq.items():
        # Smoothing: (total words seen in genre + unique seen words + 1)
        numGenreWords = totalFreq[genre] + len(freq[genre]) + 1
        wordProbs[genre] = {word:math.log(cnt / numGenreWords) for word, cnt in freqs.items()}

    # Smoothing: 1 / (total words seen in genre + unique seen words + 1)
    unseenProb = {}
    for genre in genres:
        unseenProb[genre] = math.log(1 / (totalFreq[genre] + len(freq[genre]) + 1))

    return (wordProbs, genreProbs, unseenProb)

if __name__ == '__main__':

    for category in ['genre', 'year']:
        # Define different genres (or years)
        if category == 'genre':
            genres = ['country', 'electronic', 'folk', 'hip-hop', 'indie', 'jazz', 'metal', 'pop', 'r&b', 'rock']
        # Year specific
        else:
            genres = [x for x in range(1968, 2017) if x != 1969]

        TRAIN_FROM_FILE = False 

        # Naive bayes improvements
        print('Preprocessing...')
        if REMOVE_STOP_WORDS:
            stopWords = getStopWords()
        if USE_STEMMING:
            ps = PorterStemmer()

        # Train naive bayes (smoothing for unseen prob for each genre)
        filePath = 'data/all-artists/{}/'.format(category)
        print('Training naive bayes classifier...')
        if TRAIN_FROM_FILE:
            (wordProbs, genreProbs, smoothing) = trainNBC(genres, filePath)
            picklerick = {}
            picklerick['wp'] = wordProbs
            picklerick['gp'] = genreProbs
            picklerick['sm'] = smoothing
            pickle.dump(picklerick, open('./data/all-artists/pickles/nbc-{}-train.pkl'.format(category), 'wb'))
        else:
            picklerick = pickle.load(open('./data/all-artists/pickles/nbc-{}-train.pkl'.format(category), 'rb'))
            wordProbs = picklerick['wp']
            genreProbs = picklerick['gp']
            smoothing = picklerick['sm']
        # Testing
        actual = []
        predictions = []

        # Read in test data
        print('Reading in test data...')
        with open('./data/all-artists/test.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            song_list = list(csv_reader)[1:]

        # For each song in test songs
        print('Predicting {}...'.format(category))
        for song in song_list:
            test_year = float(song[2])
            test_artist = song[3]
            test_artist_text = " ".join(song[3].lower().split('-'))
            test_genre = song[4].lower()
            test_lyrics = song[5]

            # Loop through all words probabilities
            predictSongProb = {genre: genreProbs[genre] for genre in genres}
            for line in test_lyrics.split('\n'):
                for word in line.rstrip().split(' '):
                    # Optional preprocessing
                    if REMOVE_STOP_WORDS and word in stopWords:
                        continue
                    if USE_STEMMING:
                        word = ps.stem(word)

                    # Update probability for each genre
                    for genre in genres:
                        # Smoothing: 1 / (total words seen in genre + unique seen words + 1)
                        unseenProb = smoothing[genre]
                        predictSongProb[genre] += wordProbs[genre].get(word, unseenProb)

            songPrediction = max(predictSongProb, key=predictSongProb.get)

            # Record prediction and actual
            if category == 'genre':
                actual.append(test_genre)
            else:
                actual.append(test_year)
            predictions.append(songPrediction)

        # Display results for all files
        if category == 'genre':
            print(classification_report(actual, predictions))
        else:
            rms = math.sqrt(mean_squared_error(actual, predictions))
            print('Root Mean Squared: {}'.format(rms))
