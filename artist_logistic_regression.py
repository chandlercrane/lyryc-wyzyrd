#!/usr/bin/env python

# Chandler Crane and Sophie Johnson
# Linear Regression Classifier

# Inspired By: 
# Build Your First Text Classifier in Python with Logistic Regression
# By Kavita Ganesan
# https://kavita-ganesan.com/news-classifier-with-logistic-regression-in-python/#.XfGMT5NKg00

import re
import pickle
import pandas as pd
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer

import logistic_regression as lr
TRAIN_ARTIST = False 

# Extract features using TF-IDF feature representation
def extractFeatures(field, trainData, testData):

    stopWords = set(stopwords.words('english'))
    tfidfVectorizer = TfidfVectorizer(use_idf = True, stop_words = stopWords, max_df = 0.95)
    tfidfVectorizer.fit_transform(trainData[field].values)
    
    trainFeatureSet = tfidfVectorizer.transform(trainData[field].values)
    testFeatureSet = tfidfVectorizer.transform(testData[field].values)
    
    return trainFeatureSet, testFeatureSet, tfidfVectorizer

if __name__ == '__main__':

    # Get training and testing data
    print('Reading in data...')
    allTrainData = pd.read_csv('data/top-artists/train.csv')
    allTestData = pd.read_csv('data/top-artists/test.csv')

    for artistGenre in set(allTrainData['genre'].values):
        
        # Build separate model for EACH genre
        trainData = allTrainData[allTrainData['genre'] == artistGenre]
        testData = allTestData[allTestData['genre'] == artistGenre]
        artistGenre = artistGenre.lower()
        
        # Get artists
        Y_train = trainData['artist'].values
        Y_test = testData['artist'].values

        if TRAIN_ARTIST:
            # Extract features
            print('Extracting features...')
            X_train, X_test, featureTransformer = extractFeatures('lyrics', trainData, testData)
            pickle.dump(featureTransformer, open('models/artist_{}_transformer.pkl'.format(artistGenre), 'wb'))

            # Train logistic regression artist classifier
            print('Training logistic regression artist model...')
            logReg = LogisticRegression(solver = 'liblinear', random_state = 0, C = 5, penalty = 'l2', max_iter = 1000)
            artistModel = logReg.fit(X_train, Y_train)
            pickle.dump(artistModel, open('models/artist_{}_model.pkl'.format(artistGenre), 'wb'))
        # Load from pickle
        else:
            featureTransformer = pickle.load(open('models/artist_{}_transformer.pkl'.format(artistGenre), 'rb'))
            artistModel = pickle.load(open('models/artist_{}_model.pkl'.format(artistGenre), 'rb'))
            X_test = featureTransformer.transform(testData['lyrics'].values)


        # Evaluation
        predictions = artistModel.predict(X_test)
        print('Evaluating...')
        print(artistGenre.upper())
        print(classification_report(Y_test, predictions))
