#!/usr/bin/env python

# Chandler Crane and Sophie Johnson
# Linear Regression Classifier

import re
import math
import pickle
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer

TRAIN_GENRE = False
TRAIN_YEAR = True 

# Extract features using TF-IDF feature representation
def extractFeatures(field, trainData, testData):

    stopWords = set(stopwords.words('english'))
    tfidfVectorizer = TfidfVectorizer(use_idf = True, stop_words = stopWords, min_df = 0.01, max_df = 0.95)
    tfidfVectorizer.fit_transform(trainData[field].values)
    
    trainFeatureSet = tfidfVectorizer.transform(trainData[field].values)
    testFeatureSet = tfidfVectorizer.transform(testData[field].values)
    
    return trainFeatureSet, testFeatureSet, tfidfVectorizer

if __name__ == '__main__':

    # Get training and testing data 
    trainData = pd.read_csv('data/all-artists/train.csv')
    testData = pd.read_csv('data/all-artists/test.csv')
    
    # Number of songs for each year 
    print('{:15}: {}'.format('Year', 'Number of Songs'))
    for year in set(trainData['year'].values):
        print('{:15}: {}'.format(year, len(trainData[trainData['year'] == year])))
    print()

    Y_train = trainData['year'].values
    Y_test = testData['year'].values

    # Extract features
    if TRAIN_YEAR:
        print('Extracting features...')
        X_train, X_test, featureTransformer = extractFeatures('lyrics', trainData, testData)
        pickle.dump(featureTransformer, open('models/linear_transformer.pkl', 'wb'))
        # Train logistic regression artist classifier
        print('Training linear regression year model...')
        linReg = LinearRegression()
        yearModel = linReg.fit(X_train, Y_train)
        pickle.dump(yearModel, open('models/linear_year_model.pkl', 'wb'))
    else:
        featureTransformer = pickle.load(open('models/linear_transformer.pkl', 'rb'))
        yearModel = pickle.load(open('models/linear_year_model.pkl', 'rb'))
        X_test = featureTransformer.transform(testData['lyrics'].values)

    # Evaluation
    predictions = yearModel.predict(X_test)
    print('Evaluating...')
    rms = math.sqrt(mean_squared_error(Y_test, predictions))
    print('Root Mean Squared: {}'.format(rms))
    
