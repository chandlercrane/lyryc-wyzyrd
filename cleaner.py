import sys
import os
import csv
from os import listdir
from os.path import isfile, join


class CLEANER():
    def __init__(self, files):
        self.dirty_files = files
        self.OVERWRITE = True
    def clean(self):
        for dirty_path in self.dirty_files:
            df = os.path.split(os.path.abspath(dirty_path))
            directory = df[0]
            dirty_filename, extension = os.path.splitext(df[1])
            directory = os.path.dirname(os.path.abspath(dirty_path))
            clean_filename = dirty_filename
            if not self.OVERWRITE:
                clean_filename += "-cleaned"
            clean_path = directory + '/' + clean_filename

            clean_list = []
            if extension == '.csv':
                clean_path = clean_path + extension
                with open(dirty_path) as csv_file:
                    csv_reader = csv.reader(csv_file, delimiter=',')
                    song_list = list(csv_reader)
                for song in song_list:
                    lyrics = song[5]
                    clean_lyrics = []
                    for line in lyrics.split('\n'):
                        if self.isNarration(line):
                            continue
                        line = self.tokenize(line)
                        clean_lyrics.append(line)
                    song[5] = ''.join(clean_lyrics)[:-1]
                    clean_list.append(song)
                with open(clean_path, 'w', newline='') as csv_file:
                    csv_writer = csv.writer(csv_file, delimiter=',')
                    for song in clean_list:
                        csv_writer.writerow(song)
            else:
                line_reader = open(dirty_path)
                for line in line_reader:
                    if self.isNarration(line):
                        continue
                    line = self.tokenize(line)
                    clean_list.append(line)
                line_writer = open(clean_path, 'w+')
                for line in clean_list:
                    line_writer.write(line)


    def tokenize(self, line):
        symbols = [',', '!', '?', '@', '#', '$', '%', '^', '&', '*','\n', '.']
        words = self.replaceExtraneous(line, symbols).split(' ')
        # make every word lowercase
        words = list(map(lambda w: w.lower(), words))
        return ' '.join(words) + '\n'

    def isNarration(self, line):
        if "[" in line or "(" in line:
            return True
        else:
            return False

    def replaceExtraneous(self, l, syms):
        for s in syms:
            l = l.replace(s, '')
        return l
if __name__ == '__main__':
    ## python cleaner.py <file-to-be-cleaned> ... ##

    dirty_files = sys.argv[1:]
    #paths = ['./data', './data/all-artists', './data/top-artists', './data/all-artists/genre', './data/all-artists/year', './data/top-artists/artist']
    #dirty_files = []
    for p in paths:
        dirty_files += [p + '/' + f for f in listdir(p) if isfile(join(p, f)) and f[0] is not '.']

    cleaner = CLEANER(dirty_files)
    clean_files = cleaner.clean()
