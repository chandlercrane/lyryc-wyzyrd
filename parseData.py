#!/usr/bin/env python

import csv

if __name__ == '__main__':
    with open('lyrics.csv') as csvFile:
	csvRows = csv.reader(csvFile, delimiter=',')
	line_count = 0
	for 
	for row in csvRows:
	    if line_count != 0:
		
		print('Column names are {', ".join(row)}')
		line_count += 1
	    else:
		print(f'\t{row[0]} works in the {row[1]} department, and was born in {row[2]}.')
		line_count += 1
	print(f'Processed {line_count} lines.')
