# Lyryc Wyzyrd
Chandler Crane and Sophie Johnson

Our NLP project uses multiple approaches to predict a song's genre, artist, and year of release given only lyrics. Lyrics are generated based on the predicted genre.

[Baseline Presentation](https://docs.google.com/presentation/d/1wITTUXL88Neqxp9cN5XXvUVYDqFCVinIGXDzVL-xy4s/edit#slide=id.g6fc28d0ba7_0_14)

#### Dependencies

#### Run all classification trials
To run all classification trials, simply run the script. The script will output the classification reports with f1 scores for genre and artist. It will display the root mean squared for year of release. 
```
./run.sh
```
The breakdown for each python script is described below.
1. ./analyze_data.sh - Explore our dataset. Display number of songs per genre, songs per year, and artists per genre.
2. ./ncsc.py - Run the Naive Chandler Sophie Classifier for both year and genre.
3. ./nbc.py - Run the Naive Bayes Classifier for both year and genre.
4. ./baseline\_genre\_regression - Run logistic regression on genre.
5. ./baseline\artist\_regression - Run logistic regression on artist given a genre.
6. ./baseline\_year\_regression - Run linear regression on year of release.
7. ./feature\_genre\_year_regression.py - Run logistic regression on genre and linear regression on year with four different sets of extra features.

#### Run lyrics prediction
Lyric prediction is set up to run given an input file as a command line argument. Note, this outputs a prediction for ALL genres.
```
./predict_lyrics [lyrics_file_name]
```

#### Run Lyryc Wyzyrd
Run the Lyryc Wyzyrd with an input file of lyrics as a command line argument. This program will use:
- Logistic regression using the lyrics and average number of words per line to predict the genre.
- Logistic regression using the predicted genre and lyrics to predict the artist.
- Linear regression to predict the year of release.
- 3-gram language model trained on the predicted genre to predict the next line of lyrics.
```
./lyrycs_wyzyrd [lyrics_file_name]
```