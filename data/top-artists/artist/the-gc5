when your hopes are batted down like your eyelids
where the streets all sparkle like the eyes of kids
and you rise and wipe the sleep and the stupor from your eyes
you've been cold and uncompromising
with ambitions as broad as the horizon
tunnel vision focused on a consolation prize
in your darkest hour
you're left looking at your shaky hands
and bits of carefully constructed plans
and you feel just like a stranger to everyone you see
everything they show you
says they don't even know you
so you wave goodbye to your old friends
and you greet new enemies
in your darkest hour
wasted your daylight on the death of feeling bad
stuck in this rut all afternoon
no place to go no face to know
no angel watching you
you're all alone now in your darkest hour
the minutes tick by in no man's land
and you've lost it all on a dead man's hand
your delusions are so much dust and there's no one you can trust
you're wondering when it'll ever end
you wave goodbye to old enemies and meet new friends
you pick yourself up and get back on your feet
on those sparkling streetsthe kids are hungry and nowhere to go
a thousand distractions so now you know
that the burn of youth in the old mens' eyes
never put a stop to the alibis
i've seen the fantasies and heard the praises sung
all the while looking up from the bottom rung
look down look down and see the broken pieces of humanity
beat down beat down and crushed by the heel of authority
there's no magic in your marketplace
no logic in your design
it's tragic and commonplace
the wicked leading the blind
the kids are restless and nothing to eat
a thousand fingers to point and a life of deceipt
the discourse gets so out of place
as adam smith laughs in harry browne's face
that hand is too bloody to be invisible these days
blanket comparisons ignore divergence in many waysspent a buck for every dime i earned
it's the story of my life
down on my luck and now it's time i learned
everything has it's price
but broken hearts and empty pockets
don't determine wrong and right
when the thoughts that fill your head
are the kind that make your eyeballs sweat at night
the view from the alley and the view from the valley
are empty and surrounded by dead ends
the bridges that i burned and the lessons that i learned
don't amount to a damn thing in the end
been accused and called a criminal
but i've yet to speak my mind
i may lose but the damage is minimal
and i'll soon respond in kind
i need an angel or a patron to save me
i need a message or a sign
kings and queens have set me on their assorted paths
but i'll find my way in timepound for pound we carry out threats better than anyone around
mean what we say even when we lie
the heavyweights of the insolent class
no counterweight to our whip and our cash
our needs must never be denied
always raising the stakes
always aiming low
we've got no conscience and we're gonna explode
they're putting up a strip mall where the factory used to be
we deal in violence while it is the currency
we're gonna blow
brick by brick we put up walls that are never sufficiently thick
to block off the consequences
still we save face by alluding to a mythical time and place
yearning for the days of wine and picket fences
the world is tuned in
a globalist episode
our morals are paper thin and we're gonna explode
don't look at me baby cause you know my hands are tied
i know you left me here and i'm rotting on the vine
lying well enough to make a strong man fear the meek
check your pocketbooks cause i know you got off cheapcast aside and asunder
but before they put me under
i'll make a deal with the bounty hunter
for the price upon my head
walked home to angry faces
so i laced up my laces
and i headed for far off places
though you'd rather see me dead
the saints have all looked down on me
they've shook their heads and frowned on me
they see you've made a clown out of me
i'm breakin' down
i used to strut and swagger
now i stumble 'round and stagger
from my back i'll pull your dagger
and keep it by my side
but there's no room at the stable
and there's no place at the table
but for the strong-willed and the able
while i'm knocking and denied
i twist my mouth in prayer
awake but not aware
all i got is this blank and empty stare
and every time he sees my face
he tries to shake me down again
or break me down there's no difference todayget your head out of the clouds kid
get your heart out of the gutter
get your mind off your sleeve
it's so easy to read
you used to walk so proud kid
like there was no other
like a fool you believed
you're so easily deceived
you never get just what you want
you only get what you deserve
i see you chasin' down a spectre
runnin' headlong into pain
apparitions in your eyes
she's cut you down to size
and all your good intentions
have only brought you rain
so grow up and get wise
it's cruelty that gets you byi don't come around during waking hours
to see a dog like you
you'd do everything in your power
to always stay brand new
you ain't worth my worries
you ain't worth my tears
it's like being stuck in a doghouse hangin' round here
there's those with futures and those with pasts
and can the tables turn your way
you're ticking down time 'til your husband asks
why you lay around all day
you ain't worth every penny
that i ain't got
so stick around and enjoy the mess you've wrought
i don't wanna waste my time on the firing line
waiting for the other foot to fall
i tell you honey my only crime
was talking to you through the walls
i don't come around during waking hours
i don't come around at all these days
you can call me a thief you can call me a coward
i knew well enough to get away
you ain't worth my worries
you ain't worth my tears
there's no farewells when you know you won't see me for a couple of years
i gotta run
cause the day's drawin' near
it's like being stuck in a doghouse hangin' round herei've seen the opposition
i've seen what they call clout
i've seen some days when i should have just stayed down for the count
i choose my battles i fought 'til i was done
i used to do it alone but now i'm not the only one
i've been in trouble so many time before
i've took on all the shit 'til i couldn't take no more
i bear my burden alone it weighs a ton
i used to go it alone but now i'm not the only onebacklash once again a stiff reaction to imagined stimuli
outside we are tied and you're a victim of your pride
i know just how you feel when you pretend you're all alone
lonely you're such a fraud you won't even pick up the phone
and the only thing that holds me here is my patience
so don't tell me that i'm selfish don't tell me you're abused
everthing i own everything i do all that i've been through
shows me how my ardor is misdirected
my faith in you refused
outcast looking for something someone better than me
i say look at yourself and tell me what you see
last week you were mincing words with the skill of a priest
sunday comes and goes and now you swear you're incompletewell you came up short when you staked your claim to fame
when you walked downtown and you anted up to their game
they said to set your throwaway feelings
to the ambitious sound of begging and stealing
youe been looking up the charts and your daddy record racks
they move your mouth for a while until you crumble and crack
and when your time has come and gone and youe put back on your shelf
youl find out your knee jerks just like anybody else
you look so young and full of danger
just a glorified rearranger
in bed with the money changers
kissing ass to total strangers
over here over there
a sheep in wolf clothing
too seldom laid barefire up the propaganda mills
we're going in for the kill
just as recklessly as before
i've seen you as you've filled the streets
with slogans and careless poetry
and i know how little it all means
bodies overflowing my front page
bodies crowding towards the center stage
bodies pile higher everyday
bodies all swear they got something to say
ringing in with what i don't care about
i know your ego's whereabouts
and you're too fickle for me to mind
you see the blood that's being shed
you raise your voice but nothing's said
just another body whose voice will fadei got my education in the ivory halls
found the pulse of the nation in truck stop toilet stalls
and we're running out of time because we're running out of patience
so visit our sins on future generations
you can speak or have a conscience but you know it doesn't pay
they'll keep you at the margins and they'll shut you away
i've been looking up and down and searchin' round these parts for hope
is like finding your footing on a slippery slope
it's all that i can do just to make it
and say the emperors around us are all naked
it's all that i can do just to make it through
and fight against their tendency to turn their backs on you
they envy you because you're young it's what they wanna be
but kid beware of hucksters who'll sell you slavery
they'll package inspiration like a product on a shelf
killing any inclination to find it for yourselfjohnny was a man of his word he said
"i've never been locked down before
now i'm sitting in this fucking box
and i know there's gotta be more
i don't really give a fuck about it
it's done nothing for me
i don't really give a shit about it
i've seen all there is to see
i wanna go where the action is
i wanna see the city lights
their using all their will to confine me
i won't go down without a fight
another night on the railroad tracks
and i know i've had my fill
severed arteries in a town
that's long over the hill"my self effacing charity and gluttonous prosperity
don't move mountains of misery or stone
my never-ending loyalty to hypocritical royalty
has cut my sense of belonging to the bone
you ask me what i want to be i become the sum of all i see
and grow old in confusion and disgrace
the recognition of my sins the erosion of my discipline
won't someone come and put me in my place
come and see what i've become
tied down to no one
and stuck between aspirations
now i'm thankful for my sovereignty i know it rests on poverty
and sometimes i would rather crawl than fight
i know i've gone nowhere fast by walking down the narrow path
and i cringe at battle i fought out of spite
there was a time there was a place where everybody knew my face
and everything i touched would turn to gold
but now i've come to realize that to see through all their fucking lies
is to refuse to be bought or soldi'm not theirs in product or in name
but i've got no true discretion i can speak of
you can make your own distinctions but it's all the fucking same
it's servitude for someone else's sake
and i dream i'm gonna give'em the old heave ho
and i strive to bite the hand that's feeding me at last
and carry on the banner of the working class
when i'm dead on my feet or shackled to the beat
i'm always looking back over my shoulder
they make me paranoid and relegate me to defeat
a fate that fits me like an oak box
the long hard days of dead monotony
the foreman looking down so paternal
i'll curse the fucking hours cause i know they're not for me
but for now i'll carry on on borrowed timeanother time a different page when ignorance was all the rage
i used to count my blessings to know you
it mouths like yours that spread disease
everyone in your sight is weak at the knees
waiting for your true intentions to show through
call in the exterminator this place is full of bugs
rats show their faces later when youe already pulled the plug
boys like you don wear no crowns and girls like you don wear no frowns
theye only jesters in someone else court
discontented but entertained dangerous but easily contained
looking to someone to hold their fort
nowhere to run nowhere to turn after all your bridges burn
got no shoulders you can cry on and no one you can rely on
apologies the most that youl admit
you and i can meet where the avenues hit the streets
and wel settle our score of problems once and for all
il meet you in the park an hour after dark
and il try to talk but you rather see a brawl
when your heroes and villains converge
another picture will emerge
did you ever think that this would end with such a long goodbyei've seen the better part of a worse day
i'd crawl out of this hole if i could only change my ways
i've spent some years on bended knees
a thousand kids all share a similar disease
i'll shut em off and go away
i'll live to see another day
with courage garnered from these waves
i throw a record on the table and i feel the strength
coming back over me
these songs wreckage from the past keeps me going
i'm down and out of luck and i am praying
i can't hear a word people are saying
i can't hear nothing but these songs
i try to be brave but it's been so long
i need a messenger to tell me that i'm strong
these days the airwaves are filled with lies
and all the charlatans wear the same fucking disguisebroke every bone in your body
for all i know
gone halfway to heaven before the devil's seen you go
the years are changing
from far away
and i'll wake up in nebraska on new year's day
i put the brakes on like a sinking suspicion
and tell me what have i got
no control over old libraries of useless thoughts
i think someone should hit you
they should hit you where it hurts
with sticks and stones and break your bones and leave you in the dirt
the things that they say
behind your back
are too treacherous to be anything but fact
the future's so bright that it's fight or flight
to the sound of a siren song
when you don't know better you put off what you can't prolong
wish you were here
we'd start the year on a death trip
as the clock struck midnight
in another time zone
all i could think about were your broken bones
and as that car swerved
from side to side
for the first time in a year i felt satisfied
but like every feeling i felt you felt it first
every time i hurt you felt it worse
and the times i knelt to receive your curse
are in the ditch by the side of the roadi'm feeling so low tonight
i feel like there's no respite
from the boredom in my mind
i think that i'll never see
the things that i need to be
encompassed by loneliness and time
cowardice has been the norm
fail to find perfection of form
in the everyday insignificances
which are stacked against me
with my lies and prophecies
i feel like a dying dream
flashed up on the silver screen
naked for everyone to see
i wanna know what would it be like
to become a stereotype
to become the enemy
cause when you're standing on top of the world
you watch it all fall away from youearly in the morning to an afternoon of toil and despair
you're always shooting yourself in the foot
from the food you eat right down to the clothes you wear
and the nights get long and lonely
and the sun refuses to shine
on a life that goes from day to day
with no reason and no time
all over town the people resound with "we just wanna be free"
no other way to face the day that's what it means to me
all of your life you're under the knife slaving to routine
no mistakes to make if you wanna escape looks like bars to me
they shut you up the moment that you mention your distractions and your dreams
so you shut your mouth and do the best you can
to live a life of secrecy
and you sing a song so free despite the threat of harsh remand
and you laugh to yourself at those who keep their heads buried in the sandanother day goes by you lay your head down
and you count yourself the richer for the grief
those rare moments of joy and painless laughter
will pass you by like stardust in your sleep
you've been immobilized by jurisprudence
weighing in on both sides of the scale
they've handed you less than you might have asked for
and all that they've bestowed is old and stale
outside the hustlers hustle
and philosophers wrestle
with the symptoms of our age
while you're moderating monologue
between a sucker and a sage
another day goes by you lay your head down
you can't get no satisfaction or relief
moving one step further down a line
stretching back far beyond belief
you put your letter in the box
and you hope for the best
but you can't take it back
once it's off of your chest
they only turn the clocks back once every year
for 21 years you've been aimlessly walking
down detours and down all the well-worn trails
and all the while you've been shamelessly talking
but that's how you kill the time when all else failsfrom the one-two punch of the days of '19
to the clear and present danger of the modern scene
nonsensical fools with centrist views wanna crucify their saints
and their struggle for existance is a totalitarian fate
your tradition of sedition is quickly being replaced
the rush to moderation is shocking
distinctions still put men behind bars
the mutinous at heart will bleed in their master's bloody wars
and the intelligent are in traction as the times of crisis near
will they mobilize for justice or will they mobilize for fear
you always dared to tell the truth
this one's for you eugene
had a heart and conviction in everything you'd do
canton 1918
they're so filled up with delusions i wish there were another way
but gene i gotta tell ya they'd still lock you up today
the man was cruel to you eugene draconian at best
your tradition of sedition will soon be laid to resti turned to him and said "there's gotta be a better way
than chomping at the bit wasting away"
there's no outlets and i'm blowing my fuses
drowning in despair grasping for each day
i'm running out of reasons
for running into you
negative emotions pull me through
i'm struggling to pay off all my debts
the men upstairs are calling off all bets
i'm biding my time holding out for a fairer shake
as they kick down my door in order to collect
my time will come one day you wait and see
but until that day i'll sit and ride for free
a broken windshield a broken rearview mirror
serving out my time by fate's decree
i'm running out of reasons
i keep running into you
negative emotions pull me throughin five years itl burn inside you
will you look back and say you did all that you tried to
theyl try hard each day to break you
until the day comes when lip service will forsake you
and youe been tested
pulled out your hair now
with each passing day gone by
you look the worse for wear now
your excuses all grow stale
until you see the lady in the long black veil
and every violin in the world is playing your song
but theye playing it all wrong
you think long try to get things sorted
with one foot in each grave you end up quartered
you feel a tug a subtle persuasion
as that suit and tie look ever more engaging
accept the strings they will support you
as you wave goodbye to childish things they contort youfrom this distance clarity is a laughable ideal
words shouted aloud no audible appeal
the machines keep rolling the show must go on
the kings strategically sacrifice their pawns
don't wanna play that game oh no
i don't wanna waste away
don't wanna join the ranks of the dead
i'm barely breathing on the edge i enjoy the view
from safety fingers pointed amount to nil
if the guns don't get you your silence will
must i always shirk give me some constructive work
or some meaningful responsibility
don't wanna play that game oh no
i don't wanna waste away
don't wanna join the ranks of the dead
i'm barely breathing on the edge i enjoy the viewi've seen it before and it will happen again
resources tapped production comes to an end
and if the suits paid the price wouldn't that be nice
but i know who's gonna get fucked in the end
third world armies financed by northbound drugs
murderers abound but the moralists just have to shrug
and if the suits paid the price wouldn't that be nice
but i know who's gonna get fucked in the end
it's all based on lies
what's good for them is good for you
they sell you phony dreams cause they can make them come true
and your means and ends keep crossing
and your resources are few
so what can you do
the priests of the lexicon control how the terms are defined
and they're making sure that justice is never color blind
and if the suits did the time wouldn't that be fine
but i know who's gonna get fucked in the end
and if the suits paid the price wouldn't that be nice
but i know who's gonna get fucked in the endi'll go for a walk and i'll start chewing rocks
and i'll leave all this mess in my wake
right down the block outlined in chalk
there's a man who still looks half awake
i'll tear down everything in my path
cause nothing good ever lasts
and i'm surrounded by things that have all outworn their welcomes
lifetimes are sold in houses of gold
and there ain't no way for you to win
you're out in the cold if you don't fit the mold
while they flash you their good luck grin
tear down the town tear down the town
tear down the town tear it downmy anger has acheived a sort of permanence
revenge is sweet but it's all at my expense
bipolar treatment has got me feeling blue
i'm paralyzed and i don't know what to do
so let me put up the white flag let me surrender
don't you fucking kick me when i'm down
you're so abusive i'm so reclusive
i'm already beaten to the ground
you're still complaining of a victimless crime
time's gonna heal all wounds but mine
mistakes i made as a part of growing up
all my life i've been running out of luck
said i was wrong i tried to compromise
all i could see was anger in your eyes
i try to let my guard down in this place
face to face you act in good faith
but i turn my back and i'm under attack
