import fst

class Mgram(fst.FST):
    """An m-gram language model

    data: a list of lists of symbols. They should not contain `</s>`;
          the `</s>` symbol is automatically appended during
          training
    m: value for m-gram language model
    """
    
    def __init__(self, data, m):
        super().__init__()

        curr_state = [chr(1)] * m

        # Create start and end states
        self.set_start(tuple(curr_state))

        # Create the transitions
        for words in data:
            # TESTING THIS
            curr_state = [chr(1)] * m
            for a in words:
                # Update previously seen words with new words 
                next_state = curr_state[1:]
                next_state.append(a)
                # Add transition
                self.add_transition(fst.Transition(tuple(curr_state), a, a, tuple(next_state)))
                curr_state = next_state 

        # Add accept state
        self.add_transition(fst.Transition(tuple(curr_state), fst.STOP, fst.STOP, fst.STOP))
        self.set_accept(fst.STOP)

        # Convert counts to probability
        self.probs = fst.estimate_joint(self.counts)

    def get_prob(self, t):
        if t not in self.probs:
            self.probs = fst.estimate_joint(self.counts)
        return self.probs[t]

    def add_weighted_transition(self, q, a, weight):
        # Update previously seen letters with new char
        next_state = q[1:]
        next_state.append(a)
        # Create new transition if needed
        t = fst.Transition(tuple(q), a, a, tuple(next_state))
        self.add_transition(t)
        self.counts[t] += weight

    def get_all_transitions(self, q):
        return self.transitions[tuple(q)]
