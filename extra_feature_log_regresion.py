#!/usr/bin/env python

# Chandler Crane and Sophie Johnson
# Linear Regression Classifier

# Inspired By: 
# Build Your First Text Classifier in Python with Logistic Regression
# By Kavita Ganesan
# https://kavita-ganesan.com/news-classifier-with-logistic-regression-in-python/#.XfGMT5NKg00

import re
import math
import pickle
import pandas as pd
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from sklearn.metrics import mean_squared_error
from sklearn.metrics import classification_report
from sklearn.metrics import precision_recall_fscore_support
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer

TRAIN_GENRE = True 
TRAIN_YEAR = True 

# Extract features using TF-IDF feature representation
def extractFeatures(field, trainData, testData):

    stopWords = set(stopwords.words('english'))
    tfidfVectorizer = TfidfVectorizer(use_idf = True, stop_words = stopWords, min_df = 0.001, max_df = 0.95)
    tfidfVectorizer.fit_transform(trainData[field].values)
    
    #trainFeatureSet = trainData.loc[:, ['unique', 'length']]
    #testFeatureSet = testData.loc[:, ['unique', 'length']]
    #trainFeatureSet['lyrics'] = tfidfVectorizer.transform(trainData[field].values)
    #testFeatureSet['lyrics'] = tfidfVectorizer.transform(testData[field].values)
    #trainFeatureSet = trainFeatureSet.loc[:, ['unique', 'length', 'lyrics']].values
    #testFeatureSet = testFeatureSet.loc[:, ['unique', 'length', 'lyrics']].values
    
    trainFeatureSet = tfidfVectorizer.transform(trainData[field].values)
    trainFeatureSet
    testFeatureSet = tfidfVectorizer.transform(testData[field].values)

    return trainFeatureSet, testFeatureSet, tfidfVectorizer

if __name__ == '__main__':

    print('Reading in data...')
    # Get training and testing data
    trainData = pd.read_csv('data/all-artists/train.csv')
    testData = pd.read_csv('data/all-artists/test.csv')
    print(len(trainData))

    # Add features
    trainData['length'] = trainData['lyrics'].apply(lambda x:len(x.split(' ')))
    trainData['unique'] = trainData['lyrics'].apply(lambda x:len(set(x.split(' '))))
    testData['length'] = testData['lyrics'].apply(lambda x:len(x.split(' ')))
    testData['unique'] = testData['lyrics'].apply(lambda x:len(set(x.split(' '))))

    # Number of songs in dataset
    print('{:15}: {}'.format('Genres', 'Number of Songs'))
    for genre in set(trainData['genre'].values):
        print('{:15}: {}'.format(genre, len(trainData[trainData['genre'] == genre])))
    print()

    # Extract features
    if TRAIN_GENRE or TRAIN_YEAR:
        print('Extracting features...')
        #X_train, X_test, featureTransformer = extractFeatures('lyrics', trainData, testData)
        X_train = trainData.loc[:, ['unique', 'length']].values
        X_test = testData.loc[:, ['unique', 'length']].values
        #pickle.dump(featureTransformer, open('models/extra_transformer.pkl', 'wb'))
    #else:
        #featureTransformer = pickle.load(open('models/extra_transformer.pkl', 'rb'))
        #X_test = featureTransformer.transform(testData['lyrics'].values)

    # Classify genre
    Y_train = trainData['genre'].values
    Y_test = testData['genre'].values

    # Train logistic regression genre classifier
    if TRAIN_GENRE:
        print('Training logistic regression genre model...')
        logReg = LogisticRegression(solver = 'liblinear', random_state = 0, C = 5, penalty = 'l2', max_iter = 1000)
        genreModel = logReg.fit(X_train, Y_train)
        pickle.dump(genreModel, open('models/extra_genre_model.pkl', 'wb'))
    else:
        genreModel = pickle.load(open('models/extra_genre_model.pkl', 'rb'))
        #X_test = featureTransformer.transform(testData['lyrics'].values)

    # Evaluation
    predictions = genreModel.predict(X_test)
    print('Evaluating...')
    print(classification_report(Y_test, predictions))

    # Number of songs for each year 
    print('{:15}: {}'.format('Year', 'Number of Songs'))
    for year in set(trainData['year'].values):
        print('{:15}: {}'.format(year, len(trainData[trainData['year'] == year])))
    print()

    Y_train = trainData['year'].values
    Y_test = testData['year'].values

    # Train logistic regression artist classifier
    if TRAIN_YEAR:
        print('Training logistic regression year model...')
        logReg = LogisticRegression(solver = 'liblinear', random_state = 0, C = 5, penalty = 'l2', max_iter = 1000)
        yearModel = logReg.fit(X_train, Y_train)
        pickle.dump(yearModel, open('models/extra_year_model.pkl', 'wb'))
    else:
        yearModel = pickle.load(open('models/extra_year_model.pkl', 'rb'))
        #X_test = featureTransformer.transform(testData['lyrics'].values)

    # Evaluation
    predictions = yearModel.predict(X_test)
    print('Evaluating...')
    rms = math.sqrt(mean_squared_error(Y_test, predictions))
    print('Root Mean Squared: {}'.format(rms))
