#!/usr/bin/env python

import pandas as pd
from sklearn.model_selection import train_test_split
import math

if __name__ == '__main__':

    # Get training and testing data
    data = pd.read_csv('data/all-songs-fixed.csv')
    data[data.genre.notnull()]

    ############## ACTUAL CHANGES ################
    genre_limits = {
        'Pop'           : .49425429,
        'R&B'           : .99999999,
        'Metal'         : .84178627,
        'Rock'          : .1830915,
        'Indie'         : .99999999,
        'Folk'          : .99999999,
        'Country'       : .99999999,
        'Jazz'          : .99999999,
        'Hip-Hop'       : .80499094,
        'Electronic'    : .99999999
    }

    year_limits = {
        2006    : .1375705,
        2007    : .1474187,
        2014    : .8329168,
        2008    : .53475936,
        2007    : .18991188,
        2006    : .16347348,
        2008    : .69803155
    }

    # Record songs for each genre
    genres = set(data['genre'].values)
    genreData = {}
    for genre in genres:
        genreData[genre] = data[data['genre'] == genre]
    # Balance Genres
    for genre in set(data['genre'].values):
        if genre not in genre_limits.keys():
            continue
        genreData[genre], _ = train_test_split(genreData[genre], test_size = 1 - genre_limits[genre], random_state = 2000)
    # Add back to same pandas dataframe
    data = pd.DataFrame()
    for entries in genreData.values():
        data = data.append(entries, ignore_index = True)

    #data.to_csv('data/all-songs-genre-limited.csv')

    # Record songs for each year
    years = set(data['year'].values)
    yearData = {}
    for year in years:
        if isinstance(year, float) or isinstance(year, str):
            continue
        yearData[year] = data[data['year'] == year]
    # Balance Years
    for year in years:
        if isinstance(year, float) or isinstance(year, str):
            continue
        elif year not in year_limits.keys():
            limit = .99999999
        else:
            limit = year_limits[year]
        #print(year)
        yearData[year], _ = train_test_split(yearData[year], test_size = 1 - limit, random_state = 2000)

    # Add back to same pandas dataframe
    data = pd.DataFrame()
    for entries in yearData.values():
        data = data.append(entries, ignore_index = True)
        
    #data.to_csv('data/all-songs-year-limited.csv')
    #data.to_csv('data/all-songs-limited.csv')


    ##############################################

    # Print updated song frequencies
    print('{:15}: {}'.format('Genres', 'Number of Songs'))
    genreData = {}
    for genre in set(data['genre'].values):
        genreData[genre] = data[data['genre'] == genre]
        print('{:15}: {}'.format(genre, len(genreData[genre])))
    print()

    # Display new proportions
    print('{:15}: {}'.format('Genres', 'Percentage of Songs'))
    total = sum([len(x) for x in genreData.values()])
    for genre in set(data['genre'].values):
        print('{:15}: {}'.format(genre, len(genreData[genre]) / total))
    print()

    # Print year frequencies
    print('{:15}: {}'.format('Years', 'Number of Songs'))
    total = sum([len(x) for x in genreData.values()])
    for year in set(data['year'].values):
        print('{:15}: {}'.format(year, len(data[data['year'] == year])))
    print()

    # Display number of artists in each genre
    print('{:15}: {}'.format('Genres', 'Number of Artists'))
    for genre in genres:
        g = data[data['genre'] == genre]
        numArtists = len(set(g['artist'].values))
        print('{:15}: {}'.format(genre, numArtists))
    print()
