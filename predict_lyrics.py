#!/usr/bin/env python

# Chandler Crane and Sophie Johnson
# M-Gram Model

import sys
import fst
import mgram 
import random

STOP_WORDS = set(['ourselves', 'hers', 'between', 'yourself', 'but', 'again', 'there', 'about', 'once', 'during', 'out', 'very', 'having', 'with', 'they', 'own', 'an', 'be', 'some', 'for', 'do', 'its', 'yours', 'such', 'into', 'of', 'most', 'itself', 'other', 'off', 'is', 's', 'am', 'or', 'who', 'as', 'from', 'him', 'each', 'the', 'themselves', 'until', 'below', 'are', 'we', 'these', 'your', 'his', 'through', 'don', 'nor', 'me', 'were', 'her', 'more', 'himself', 'this', 'down', 'should', 'our', 'their', 'while', 'above', 'both', 'up', 'to', 'ours', 'had', 'she', 'all', 'no', 'when', 'at', 'any', 'before', 'them', 'same', 'and', 'been', 'have', 'in', 'will', 'on', 'does', 'yourselves', 'then', 'that', 'because', 'what', 'over', 'why', 'so', 'can', 'did', 'not', 'now', 'under', 'he', 'you', 'herself', 'has', 'just', 'where', 'too', 'only', 'myself', 'which', 'those', 'i', 'after', 'few', 'whom', 't', 'being', 'if', 'theirs', 'my', 'against', 'a', 'by', 'doing', 'it', 'how', 'further', 'was', 'here', 'than', '<\\s>'])


# Predict most probable word given last m words 
def predict_word(model, m_words, seen, genre_words, curr_len, avg_len, is_chorus):
    max_prob = float('-inf')
    max_word = ' ' 
    possible_words = {}

    # Find next word with maximum probability
    options = model.get_all_transitions(m_words)
    for word, transitions in options.items():
        for t in transitions:
            prob = model.get_prob(t)
            if prob > max_prob:
                max_prob = prob
                max_word = t.a 
            possible_words[t.a] = prob

    # Order all possibilities by probability 
    possible_words = sorted(possible_words.items(), key=lambda kv: kv[1], reverse=True)

    # Prioritize stop char if too long
    if curr_len >= avg_len - 1 and '<\\s>' in possible_words:
        return '<\\s>'

    # Restrict to words seen most often in n - 1 lines (no stop words)
    top_words = [x[0] for x in possible_words if x[0] in seen]

    if len(top_words) != 0:
        top_words = sorted(top_words, key=lambda x: seen[x], reverse=True)
    # If no seen words, just use all possibilities
    else: 
        top_words = [x[0] for x in possible_words]

    # If no options, add random word
    if len(top_words) == 0:
        top_words.append(random.choice(tuple(seen.items()))[0])

    # Return random from top 3
    if len(top_words) > 3:
        top_words = top_words[:3]
    return random.choice(top_words)

def get_score(line, seen, avg):
    score = 0 
    for word in set(line.split()):
        score += seen.get(word, 0)
    n_words = len(line.split())
    if n_words < avg - 4 or n_words > avg + 4:
        score = 0 
    return score

if __name__ == '__main__':

    # Get arguments
    genres = ['country', 'electronic', 'folk', 'hip-hop', 'indie', 'jazz', 'metal', 'pop', 'r&b', 'rock']
    for genre in genres:
        print(genre.upper())
        m = 3 
        training_file = 'data/all-artists/genre/' + genre
        testing_file = 'test.txt'

        # Record all words in genre
        genre_words = {}

        # Read in data from file
        data = []
        for line in open(training_file):
            wordList = list(line.rstrip().split())
            # Track most frequent words in genre
            for word in wordList:
                if word not in STOP_WORDS:
                    genre_words[word] = genre_words.get(word, 0) + 1
            wordList.append('<\s>')
            data.append(wordList)

        # Create m gram model 
        model = mgram.Mgram(data, m)
        m_words = [chr(1)] * m 

        # Weight to give transitions in n - 1 lines
        weight = max(model.counts.values())

        # Record seen words (not stop words)
        visited_states = set()
        seen_words = {}

        # Calculate average number of words in line
        total_words = 0
        num_lines = 0 

        # Read in first n - 1 lines
        for line in open(testing_file):

            # Track last state as visited
            m_memory = m_words
            visited_states.add(tuple(m_memory))

            # Initialize for new line
            m_words = [chr(1)] * m 
            wordList = list(line.rstrip().split())
            wordList.append('<\s>')

            for word in wordList:
                # Use language model to predict next word 
                model.add_weighted_transition(m_memory, word, weight)    
                model.add_weighted_transition(m_words, word, 1)    

                # Record seen words
                if word not in STOP_WORDS:
                    seen_words[word] = seen_words.get(word, 0) + 1

                # Update state
                m_words = m_words[1:]
                m_words.append(word)
                m_memory = m_memory[1:]
                m_memory.append(word)
                total_words += 1
            num_lines += 1
        model.probs = fst.estimate_joint(model.counts)

        # Average line length
        avg_length = total_words / num_lines

        # Record final state
        last_state = m_memory
        all_predictions = set()

        # If chorus, no randomization
        is_chorus = False
        N = 10
        if tuple(last_state) in visited_states:
            is_chorus = True
            N = 1

        # Predict nth line N times
        for _ in range(N):

            # Avoid cycles
            visited_states = set()

            # Re-initialize for prediction
            predicted_line = []

            # If line has been seen before, repeat
            m_words = [chr(1)] * m 
            if is_chorus:
                m_words = last_state

            # Guess up to 50 words
            iterations = 200 
            curr_length = 0
            is_valid = True
            while iterations > 0:

                # Use language model to predict next word 
                predicted_word = predict_word(model, m_words, seen_words, genre_words, curr_length, avg_length, is_chorus)
                
                # Break if cycle or longer than 1.5 average length
                if tuple(m_words) in visited_states or len(predicted_line) > 1.5 * avg_length:
                    is_valid = False
                    break
                # Break if stop char found
                elif predicted_word == '<\s>':
                    break

                visited_states.add(tuple(m_words))
                predicted_line.append(predicted_word)
                m_words = m_words[1:]
                m_words.append(predicted_word)
                curr_length += 1
                iterations -= 1

            # Print predicted line
            if is_valid:
                prediction = ' '.join(predicted_line)
                all_predictions.add(prediction)
         
        if len(all_predictions) > 0:
            best_prediction = max(all_predictions, key=lambda x: get_score(x, seen_words, avg_length))
        else:
            best_prediction = 'N/A'
        # Print predicted line
        print('=' * 50)
        print(best_prediction)
        print('=' * 50)

        #print('=' * 50)
        #for i, p in enumerate(all_predictions):
        #    print('{}) {}'.format(i, p))
        #print('=' * 50)
