import sys
import csv
import random

class Counter():
    def __init__(self):
        self.counts = {}
        self.total = 0
        self.genres = ['country', 'electronic', 'folk', 'hip-hop', 'indie', 'jazz', 'metal', 'pop', 'r&b', 'rock']
        self.song_list = []

    def setup(self, file):
        for genre in self.genres:
            self.counts[genre] = 0
        with open(file) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            self.song_list = list(csv_reader)

    def countLines(self, file):
        self.setup(file)

        for song in self.song_list:
            index = song[0]
            title = " ".join(song[1].lower().split('-'))
            year = song[2]
            artist = song[3]
            artist_text = " ".join(song[3].lower().split('-'))
            genre = song[4].lower()
            lyrics = song[5]
            self.counts[genre] += 1
            self.total += 1

    def resetCounts(self):
        for genre in self.counts.keys():
            self.counts[genre] = 0
        self.total = 0

    def showCounts(self, file):
        self.countLines(file)
        for genre in self.counts.keys():
            print("{:10} {:6} {}".format(genre, self.counts[genre], self.counts[genre]/self.total))

class Toolkit():
    def __init__(self):
        self.counts = {}
        self.songs = {}
        self.genres = ['country', 'electronic', 'folk', 'hip-hop', 'indie', 'jazz', 'metal', 'pop', 'r&b', 'rock']
        self.reset()
        self.counter = Counter()

    def reset(self):
        for genre in self.genres:
            self.counts[genre] = 0
            self.songs[genre] = []

    def showCounts(self, file):
        self.counter.showCounts(file)

    def limitSongs(self, file, SONG_LIMIT):
        with open(file) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            self.song_list = list(csv_reader)
        for song in self.song_list:
            index = song[0]
            title = " ".join(song[1].lower().split('-'))
            year = song[2]
            artist = song[3]
            artist_text = " ".join(song[3].lower().split('-'))
            genre = song[4].lower()
            lyrics = song[5]
            self.songs[genre].append(song)

        for g in self.genres:
            if len(self.songs[g]) > SONG_LIMIT:
                self.songs[g] = random.sample(self.songs[g], SONG_LIMIT)
                #self.songs = list(random.shuffle(self.songs[g]))[:SONG_LIMIT-1]

    def limitFile(self, input, output, SONG_LIMIT):
        self.reset()
        self.limitSongs(input, SONG_LIMIT)
        with open(output, 'w', newline='') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=',')
            for g in self.songs.keys():
                for s in self.songs[g]:
                    csv_writer.writerow(s)

    def zipFiles(self, in1, in2, out):
        songs = []
        with open(in1) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            songs += list(csv_reader)
        with open(in2) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            songs += list(csv_reader)
        with open(out, 'w', newline='') as csv_file:
        	csv_writer = csv.writer(csv_file, delimiter=',')
        	for song in songs:
        		csv_writer.writerow(song)

    def splitFile(self, file, splitPercent, out1, out2):
        songs = []
        with open(file) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            songs += list(csv_reader)

        split = int(len(songs) * splitPercent)
        random.shuffle(songs)

        s1 = songs[:split]
        s2 = songs[split:]

        with open(out1, 'w', newline='') as csv_file:
        	csv_writer = csv.writer(csv_file, delimiter=',')
        	for s in s1:
        		csv_writer.writerow(s)

        with open(out2, 'w', newline='') as csv_file:
        	csv_writer = csv.writer(csv_file, delimiter=',')
        	for s in s2:
        		csv_writer.writerow(s)

    def splitByParam(self, file, param, dir):
        self.reset()
        with open(file) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            self.song_list = list(csv_reader)
        for song in self.song_list:
            index = song[0]
            title = " ".join(song[1].lower().split('-'))
            year = song[2]
            artist = song[3]
            artist_text = " ".join(song[3].lower().split('-'))
            genre = song[4].lower()
            lyrics = song[5]
            self.songs[genre].append(song)
        for genre in self.songs.keys():
            with open(dir+genre, 'w', newline='') as csv_file:
            	csv_writer = csv.writer(csv_file, delimiter=',')
            	for song in self.songs[genre]:
            		csv_writer.writerow(song)

if __name__ == '__main__':

    f1 = './data/all-artists/train.csv'
    f2 = './data/all-artists/test.csv'
    master = './data/all-artists/master.csv'
    output = './data/all-artists/master-limited.csv'

    toolkit = Toolkit()
    #toolkit.limitFile(master, output, 20000)
    #toolkit.showCounts(output)

    dir = './data/all-artists/genre/'
    toolkit.splitByParam(f1, 'genre', dir)
