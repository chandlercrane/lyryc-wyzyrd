#!/usr/bin/env python

# Chandler Crane and Sophie Johnson
# Naive Chandler and Sophie Classifier

# NLP Project: Baseline
# 1) Count word frequencies for each genre given a text file with lyrics.
# 2) Calculate log scores given the word frequencies for each genre.
# 3) Find score of each genre in test data by adding its log score for that word.

import os
import math
import csv
import pickle
from sklearn.metrics import mean_squared_error
from sklearn.metrics import classification_report
from nltk.stem import PorterStemmer

def trainNCSC(genres, filePath):

    freq = {genre:{} for genre in genres}
    score = {genre:{} for genre in genres}
    totalFreq = {}

    # Record word frequencies for each genre
    for genre in genres:
        for line in open(filePath + str(genre)):
            for word in line.rstrip().split():
                freq[genre][word] = freq[genre].get(word, 0) + 1
                totalFreq[word] = totalFreq.get(word, 0) + 1

    # Calculate scores
    for genre, freqs in freq.items():
        score[genre] = {word:math.log(cnt/totalFreq[word]) for word, cnt in freqs.items()}

    return (score, totalFreq)

if __name__ == '__main__':

    for category in ['genre', 'year']:
        # Define different genres (or years)
        if category == 'genre':
            genres = ['country', 'electronic', 'folk', 'hip-hop', 'indie', 'jazz', 'metal', 'pop', 'r&b', 'rock']
        else:
            genres = [x for x in range(1968, 2016) if x != 1969]

        TRAIN_FROM_FILE = False 
        filePath = 'data/all-artists/{}/'.format(category)
        print('Training naive chandler & sophie classifier...')
        if TRAIN_FROM_FILE:
            # Train naive bayes (smoothing for unseen prob for each genre)
            (score, totalFreq) = trainNCSC(genres, filePath)
            picklerick = {}
            picklerick['sc'] = score
            picklerick['tf'] = totalFreq
            pickle.dump(picklerick, open('./data/all-artists/pickles/ncsc-{}-train.pkl'.format(category), 'wb'))
        else:
            picklerick = pickle.load(open('./data/all-artists/pickles/ncsc-{}-train.pkl'.format(category), 'rb'))
            score = picklerick['sc']
            totalFreq = picklerick['tf']

        # Using training data to find scores (totalFreq used for smoothing)
        numCorrect = 0
        numTotal = 0
        actual = []
        predictions = []

        # Get list of songs to test
        print('Reading in test data...')
        with open('./data/all-artists/test.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            song_list = list(csv_reader)[1:]

        # Test each song
        print('Predicting {}...'.format(category))
        for song in song_list:
            test_year = float(song[2])
            test_artist = song[3]
            test_artist_text = " ".join(song[3].lower().split('-'))
            test_genre = song[4].lower()
            test_lyrics = song[5]

            predictSongScore = {genre:0 for genre in genres}

            for line in test_lyrics.split('\n'):
                for word in line.rstrip().split(' '):
                    totalWordFreq = totalFreq.get(word, len(totalFreq))

                    # Update probability for each genre
                    for genre in genres:
                        # Smoothing: 1 / (total words seen in genre + unique seen words + 1)
                        predictSongScore[genre] += score[genre].get(word, math.log(1/totalWordFreq))

            songPrediction = max(predictSongScore, key=predictSongScore.get)

            # Record prediction and actual
            if category == 'genre':
                actual.append(test_genre)
            else:
                actual.append(test_year)
            predictions.append(songPrediction)

        # Display results for all files
        if category == 'genre':
            print(classification_report(actual, predictions))
        else:
            rms = math.sqrt(mean_squared_error(actual, predictions))
            print('Root Mean Squared: {}'.format(rms))
